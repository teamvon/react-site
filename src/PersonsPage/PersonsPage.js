import React, { Component } from 'react';
import PersonsList from '../PersonList/PersonList'
import { Divider, Button, Drawer, Input } from 'antd'
import uuid from 'uuid'
class PersonsPage extends Component {

    state = {
        person: [
            {
                id: uuid(),
                name: "nick",
            },
            {
                id: uuid(),
                name: "ross",
            },
            {
                id: uuid(),
                name: "jesus",
            },
            {
                id: uuid(),
                name: "dave",
            },
        ],
        employees: [
            {
                id: uuid(),
                name: "nick"
            },
        ],
        visible: false,
        newEmployeeName: ""
    }

    removePerson = (id, type) => {

        var newListOfPeople = this.state[type].filter((object) => {
            return object.id != id
        });

        this.setState({
            [type]: newListOfPeople
        });
    }

    toggleList = () => {
        this.setState({
            visible: !this.state.visible
        });
    }

    updateNewEmployee = (e) => {
        var name = e.target.value;
        this.setState({
            newEmployeeName: name
        })

    }
    addEmployee = () => {
        if(this.state.newEmployeeName == "")
            return;

        var newEmployee = {
            id: uuid(),
            name: this.state.newEmployeeName
        }
        this.setState({
            employees: [...this.state.employees, newEmployee],
            newEmployeeName: "",
        })
    }
    render() {
        return (
            <div className={"containerDiv"}>
                <Input
                    value={this.state.newEmployeeName}
                    onChange={(e) => { this.updateNewEmployee(e) }}
                />
                <Button type="primary"
                    onClick={() => { this.addEmployee() }}
                >
                    save employee
                </Button>

                <Button type="primary"
                    onClick={() => { this.toggleList() }}
                >
                    {this.state.visible ? "Hide" : "Show"} List
                </Button>
                {this.state.visible ?
                    <div>
                        <Divider>People</Divider>
                        <PersonsList removePerson={this.removePerson.bind(this)} type={"person"} peopleList={this.state.person} />
                        <Divider>Employees</Divider>
                        <PersonsList removePerson={this.removePerson.bind(this)} type={"employees"} peopleList={this.state.employees} />
                    </div>
                    :
                    ""
                }
            </div>
        )
    }
}

export default PersonsPage;
