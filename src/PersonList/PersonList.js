import React, { Component } from 'react';
import { List } from 'antd'


class PersonList extends Component {
  state = {
    people: [{}]
  }

  componentDidMount() {
    this.setState({
      people: this.props.peopleList
    })
  }

  componentWillReceiveProps(newProps) {
    if(this.state.people != newProps.peopleList){
      this.setState({
        people: newProps.peopleList
      });
      console.log(newProps);
    }
  }

  render() {
    return (
      <List
        dataSource={this.state.people}
        renderItem={person => (
          <List.Item>{person.name} {person.id} <a onClick={() => {this.props.removePerson(person.id, this.props.type)}}>remove</a></List.Item>
        )}
      />
    )
  }
}

export default PersonList;
