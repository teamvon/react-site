import React, { Component } from 'react';
import logo from './logo.svg';
import 'antd/dist/antd.css';
import './App.css';
import PersonList from './PersonList/PersonList'
import PersonsPage from './PersonsPage/PersonsPage'

class App extends Component {
  render() {
    return (
      <div className="App">
        <PersonsPage />
      </div>
    );
  }
}

export default App;
